$(document).ready(function () {
  $('.left_bar_list_mobile, .mobile_video_slider,.solution_list').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false
  });
  $('.solution_list').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '50px',
    variableWidth: true,
    autoplay: true
  });
});
