$(document).ready(function () {
  $('.slider_list').slick({
    infinite: true,
    arrows: true,
    centerMode: true,
    cssEase: 'linear',
    lazyLoad: 'progressive',
    respondTo: 'slider',
    variableWidth: true
  });
});