$(document).ready(function(){
  $('.mobile_dropdown').on('click', function(e){
    e.preventDefault();
    if(!$(this).hasClass('active')){
      $('.product_first_mobile').css('margin-top', '170px').addClass('active');
      $(this).addClass('active').css('transform', 'rotate(90deg)');
    }else{
      $('.product_first_mobile').css('margin-top', '-120px').removeClass('active');
      $(this).removeClass('active').css('transform', 'rotate(-90deg)');
    }
  })
  $('.header_menu_mobile').on('click', function(){

    var logo = $(".header_logo"),
      btn = $('.header_buy'),
      btnMobile = $('.header_buy_collapse'),
      headerNav = $('.header_nav'),
      search = $('.header_form_search__mobile'),
      productMobile = $('.product_first_mobile');

    if(!$(this).hasClass('active')){
      logo.css({
        'max-width': '150px',
        'margin': '0 45px'
      }).addClass('active');
      btn.css('display', 'none').addClass('active');
      btnMobile.css('display', 'block').addClass('active');
      headerNav.stop(true).slideUp().addClass('active');
      search.css('right', '32%').addClass('active');
      productMobile.css('margin-top', '-200px').addClass('active');
      $(this).addClass('active');
      $('.header_mobile').addClass('active');
    }else{
      logo.css({
        'max-width': '200px',
        'margin': 'auto'
      }).removeClass('active');
       btn.css('display', 'block').removeClass('active');
       btnMobile.css('display', 'none').removeClass('active');
       headerNav.stop(true).slideDown().removeClass('active');
       search.css('right', '5%').removeClass('active');
       productMobile.css('margin-top', '-120px').addClass('active');
      $(this).removeClass('active');
      $('.header_mobile').removeClass('active');
    }
    
    
  })
});