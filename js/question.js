$(document).ready(function () {
$('.question_link').on('click', function (e) {
  e.preventDefault();

  var $this = $(this),
    container = $this.closest('.section_content'),
    items = $this.closest('.question_item'),
    content = container.find('.question_content'),
    currentContent = items.find('.question_content'),
    duration = 500;

  if (!container.data('isAnimated')) {

    container.data('isAnimated', true);

    if (!items.hasClass('active')) {
      content.slideUp();
      currentContent.slideDown(duration, function () {
        container.data('isAnimated', false);
      });
      items.addClass('active').siblings().removeClass('active');

    } else {

      currentContent.slideUp(function () {
        container.data('isAnimated', false);
      });
      items.removeClass('active');
    }
  }
});