$(document).ready(function(){
  $('.slider_list').slick({
    autoplay: true,
    autoplaySpeed: 0,
    infinite: true,
    arrows: false,
    centerMode: true,
    cssEase: 'linear',
    lazyLoad: 'progressive',
    respondTo: 'slider',
    variableWidth: true
  });  
});