$(document).ready(function () {
  var menu = $(".header_menu_mobile"),
      nav = $(".header_nav"),
      buy = $(".header_buy"),
      logo = $(".header_logo"),
      search = $(".header_form_search__mobile"),
      collapseBtn = $(".header_buy_collapse");

  menu.on("click", function () {
     if(!(nav.hasClass("active"))) {
       menu.css("top", "10%");
       nav.slideDown().addClass("active");
       buy.css("display", "block");
       logo.removeClass("active").css({"margin": "auto", "max-width": "200px"});
       search.removeClass("active").css({"right": "5%", "top": "10%"});
       collapseBtn.removeClass("active").css("display", "none");
     } else {
       nav.slideUp().removeClass("active");
       menu.css({"top": "20%"});
       logo.addClass("active").css({"margin": "0 0 0 45px", "max-width": "150px"});
       search.addClass("active").css({"right": "32%", "top": "20%"});
       buy.css("display", "none");
       collapseBtn.addClass("active").css({"display": "block", "top": "20%"});
     }
  });
});
